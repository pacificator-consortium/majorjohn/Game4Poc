using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Unity.Netcode;


namespace ProjectNamespace
{
    public class UIDocumentVersion : MonoBehaviour
    {

        public string version = "SNAPSHOT";

        void Start()
        {
            // Each editor window contains a root VisualElement object
            var uiDocument = GetComponent<UIDocument>();
            VisualElement root = uiDocument.rootVisualElement;


            // Update version
            Label versionLabel = root.Q<Label>("versionLabel");
            versionLabel.text = version;

            Button stopButton = root.Q<Button>("stopButton");
            stopButton.RegisterCallback<ClickEvent>(StopButtonListener);
        }


        void StopButtonListener(ClickEvent evt)
        {
            var playerObject = NetworkManager.Singleton.SpawnManager.GetLocalPlayerObject();
            var player = playerObject.GetComponent<ShipMovement>();
            player.SubmitTargetMoveServerRpc(playerObject.transform.position);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}