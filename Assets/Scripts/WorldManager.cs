using UnityEngine;
using Unity.Netcode;
using System;
using Unity.Netcode.Transports.UTP;
using Unity.Netcode.Transports.UNET;

namespace ProjectNamespace
{
    public class WorldManager : MonoBehaviour
    {
        void OnGUI()
        {
            GUILayout.BeginArea(new Rect(10, 10, 300, 300));
            if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
            {
                StartButtons();
            }
            else
            {
                StatusLabels();
            }

            GUILayout.EndArea();
        }

        static void SetUpConnectionData()
        {
            string configPath = Application.dataPath + "/Config/config.json";
            Debug.Log("Try to load config file: " + configPath);
            Config config = new ConfigReader(configPath).loadConfig();
            Debug.Log("Config load, IP: " + config.ip + " Port: " + config.port);

            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = config.ip;           
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectPort = Convert.ToUInt16(config.port);

        }
        static void StartButtons()
        {
            //if (GUILayout.Button("Host")) NetworkManager.Singleton.StartHost();
            if (GUILayout.Button("Client")) NetworkManager.Singleton.StartClient();         
            if (GUILayout.Button("Server")) NetworkManager.Singleton.StartServer();

            // Read config file to setup ip and port
            SetUpConnectionData();
        }

        static void StatusLabels()
        {
            var mode = NetworkManager.Singleton.IsHost ?
                "Host" : NetworkManager.Singleton.IsServer ? "Server" : "Client";

            GUILayout.Label("Transport: " +
                NetworkManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
            GUILayout.Label("Mode: " + mode);
        }

    }
}