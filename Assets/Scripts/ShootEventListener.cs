using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace ProjectNamespace
{
    public class ShootEventListener : NetworkBehaviour
    {

        public GameObject tirPrefabs;

        // Start is called before the first frame update
        void Start()
        {

        }

        [ServerRpc(RequireOwnership = true)]
        public void SubmitSpawnShootServerRpc(Vector3 forwardVector, ServerRpcParams rpcParams = default)
        {
            GameObject go = Instantiate(tirPrefabs, transform.position, transform.rotation);
            go.GetComponent<NetworkObject>().Spawn();
            go.GetComponent<ShootMovement>().movementVector.Value = forwardVector;
            //SubmitMovementVectorServerRpc(forwardVector);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 forwardVector = transform.rotation * Vector3.up;
                //GameObject clone = Instantiate(tirPrefabs, transform.position, transform.rotation);
                //clone.GetComponent<ShootMovement>().movementVector = forwardVector;
                if (!NetworkManager.Singleton.IsServer)
                {
                    SubmitSpawnShootServerRpc(forwardVector);
                }

            }
        }
    }

}