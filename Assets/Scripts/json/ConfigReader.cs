using Newtonsoft.Json;
using System.IO;

public class ConfigReader
{
    private readonly string _sampleJsonFilePath;

    public ConfigReader(string sampleJsonFilePath)
    {
        _sampleJsonFilePath = sampleJsonFilePath;
    }

    public Config loadConfig() 
    {
        Config config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(_sampleJsonFilePath));
        return config;
    }
}