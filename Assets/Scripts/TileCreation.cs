using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileCreation : MonoBehaviour
{


    public Tile highlightTile;
    public Tilemap highlightMap;

    private Vector3Int previous;

    // Start is called before the first frame update
    void Start()
    {
        highlightMap = GameObject.Find("HexaColorTilemap").GetComponent<Tilemap>();
    }

    // Update is called once per frame
    void Update()
    {
        // get current grid location
        Vector3Int currentCell = highlightMap.WorldToCell(transform.position);


        // if the position has changed
        if (highlightMap == null)
        {
            Debug.Log("C'est la tuile highlightMap est null");
        }
        else
        {
            if (currentCell != previous)
            {
                // set the new tile
                highlightMap.SetTile(currentCell, highlightTile);

                // erase previous
                // highlightMap.SetTile(previous, null);

                // save the new position for next frame
                previous = currentCell;
            }
        }
    }

}
