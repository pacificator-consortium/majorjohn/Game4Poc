using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace ProjectNamespace
{
    public class ShootMovement : NetworkBehaviour
    {

        public NetworkVariable<Vector3> movementVector = new NetworkVariable<Vector3>();

        // Start is called before the first frame update
        void Start()
        {
            //GameObject player = GameObject.FindGameObjectWithTag("Player");
            //Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }


        // Update is called once per frame
        void Update()
        {
            transform.position += movementVector.Value * 10 * Time.deltaTime;
        }
    }
}