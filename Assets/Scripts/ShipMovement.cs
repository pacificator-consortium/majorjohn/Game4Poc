using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;


namespace ProjectNamespace
{
    public class ShipMovement : NetworkBehaviour
    {

        // ================== Server Side ==================

        public NetworkVariable<Vector3> target = new NetworkVariable<Vector3>();
        public NetworkVariable<float> speed = new NetworkVariable<float>(0);


        [ServerRpc]
        public void SubmitTargetMoveServerRpc(Vector3 targetParam, ServerRpcParams rpcParams = default)
        {
            setTarget(targetParam);
        }

        [ServerRpc(RequireOwnership = true)]
        private void SubmitTargetStopServerRpc(ServerRpcParams rpcParams = default)
        {
            speed.Value = 0f;
        }

        public void setTarget(Vector3 targetParam)
        {
            target.Value = targetParam;
            speed.Value = 1f;
        }


        // ================== Client Side ==================

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (speed.Value > 0)
            {

                if (Vector3.Distance(transform.position, target.Value) < 0.001f)
                {
                    transform.position = target.Value;
                    if (this.IsOwner)
                    {
                        this.SubmitTargetStopServerRpc();
                    }
                }
                else
                {
                    // Ce calcul pourrait etre econmiser (Il n'est necessaire que la premi�re fois)
                    transform.rotation = Quaternion.LookRotation(Vector3.forward, target.Value - transform.position);

                    var step = speed.Value * Time.deltaTime; // calculate distance to move
                    Vector3 temporaryVector = Vector3.MoveTowards(transform.position, target.Value, step);
                    temporaryVector.z = transform.position.z;
                    transform.position = temporaryVector;

                    if (Vector3.Distance(transform.position, target.Value) < 0.001f)
                    {
                        transform.position = target.Value;
                        if (this.IsOwner)
                        {
                            this.SubmitTargetStopServerRpc();
                        }
                    }
                }
            }
        }


    }
}