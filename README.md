# Code source du Poc pour Game4

Requière: Unity 2021.3.15f1

Tentative d'implémentation du Game4, voir : https://gitlab.com/pacificator-consortium/majorjohn/game4 (Private repository)

# Demarrer le jeu

Il faut lancer plusieurs fenêtre. 1 Pour l'hôte et 2 pour les joueurs (Afin de verifier le networking).

La selection du mode Hôte ou Joueur se fait pour l'instant via des boutons en haut à gauche de la fenêtre de jeu.

## Démarrer un Host & un Client rapidement
Pour lancer dans une fenêtre en séparer : File > Build and run (= Ctrl + B)
Puis lancer le mode interne à unity via le play. 
Il est impossible de lancer plus de build.

## Démarrer un Host & deux Client

Une fois le projet build un executable est à disposition dans le dossiers Build à la racine du projet. Il suffit de démarrer le .exe trois fois.

/!\ Attention /!\ Le fichier de configuration d'ip doit manuellement être ajouté après le build. Celui-ci est trouvable dans Assets/Config/config.json. Le dossier Config doit être copier à l'interieur du dossier "[ProjectName]_Data" de build.

### Tips

Bien penser à fermer les fenêtres avant de relancer un build.

# Déployer une version :

Utiliser le script deployVersion.sh celui-ci prend en argument le nom de la version. (Attention, cela ne mets pas la version en jeu automatiquement à jour)

```
./deployVersion.sh 0.0.1-SNAPSHOT
```

## Recuperer une version :

Le script deployVersion déploie le zip sur draftcorporation, le zip est récupérable via l'url suivante : 
https://draftcorporation.fr/download/game4%2Fgame4-0.0.1-SNAPSHOT.zip
Le numéro de version est à adapté en conséquence.

# To Do List :

# [TBD]
- [ ] Système de destruction d'ennemi

# [X.X.X]

- [ ] Persistance des données
	- [ ] Création d'un MicroService dédier
		- Conception : 
			- [ ] MDD à faire
			- [ ] Choix de techno
	- [ ] Protocole de communication
	- [ ] Utilisation du MicroService dans le jeu
- [ ] Dedicated server
- [ ] Association d'un pseudo au vaisseau
	- [ ] Chaine de carractère
	- [ ] Scène supplémentaire avec textfield pour selection du pseudo

# [0.0.2]

- [ ] Système de zoom
- [ ] Limite de terrain
- [ ] ? Fond de carte
- [ ] Proportion
- [ ] Capture d'un hexagone
	- [ ] Temps écouler dedans capture l'hexagone (Configurable)
	- [ ] Fade-in/Fade-out pendant la capture
	- [ ] Système de contestation (Le point n'est pas capturable quand il y a un ennemi dessus ?)
- [ ] Assignation d'un camps à la connection

# [0.0.1] [RELEASED] : https://draftcorporation.fr/download/game4%2Fgame4-0.0.1.zip
- [X] Déplacement
	- [X] Le vaisseau va dans la direction du dernier clic droit
		- [X] Direction
		- [X] Rotation
		- [ ] ~~ Vitesse ~~ -> Trop tôt pour être pertinant (Manque la notion de proportion)
	- [X] Système d'arrêt
- [X] Grille Hexagonal
	- [X] Utilisation d'une grille hexagonal
	- [X] Colorisation de la grille hexagonal afin de materialiser les camps
- [X] Ajout UI version 
- [X] Système de récupération d'une version
- [X] Test du networking (Sur un autre pc pour pouvoir déployer le jeu)
	- [X] gestion de l'ip et du port dans un fichier accessible, externe à l'IDE
	- [X] Test sur un réseau local

# [Bug]

- Mineur :
	- [ ] Le vaisseau se retour lors de l'arrêt