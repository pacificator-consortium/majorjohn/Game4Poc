#!/bin/bash

if [ -z "$1" ]
then
    echo "La version doit être passer en argument du script Ex: 0.0.1-SNAPSHOT"
else
	mkdir -p dist
	rm -f dist/*.zip
    zip -r dist/game4-$1.zip Build/Other
	scp dist/game4-$1.zip poulain@5.135.181.214:/home/poulain/draftcorporation/files/game4/game4-$1.zip 
fi

